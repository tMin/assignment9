//
//  ViewController.m
//  Assignment9
//
//  Created by thomas minshull on 2016-03-09.
//  Copyright © 2016 thomas minshull. All rights reserved.
//

#import "ConversationViewController.h"
#import "Backendless.h"
#import "Comment.h"

@interface ConversationViewController ()

@end

@implementation ConversationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated {
    [backendless.persistenceService save:[Comment commentWithMessage:@"I'm in!" authorEmail:@"thomasminshull@gmail.com"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
