//
//  comment.m
//  Assignment9
//
//  Created by thomas minshull on 2016-03-10.
//  Copyright © 2016 thomas minshull. All rights reserved.
//

#import "Comment.h"

@implementation Comment

+(Comment *)commentWithMessage:(NSString *)message authorEmail:(NSString *)authorEmail
{
    Comment *comment = [[Comment alloc] init];
    comment.message = message;
    comment.authorEmail = authorEmail;
    return comment;
}

@end
