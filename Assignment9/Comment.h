//
//  comment.h
//  Assignment9
//
//  Created by thomas minshull on 2016-03-10.
//  Copyright © 2016 thomas minshull. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *authorEmail;

+(Comment *)commentWithMessage:(NSString *)message authorEmail:(NSString *)authorEmail;
@end
